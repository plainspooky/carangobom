Carangobom
---

O "Carangobom" é uma API REST escrita em Python e usando Django
com Django REST Framework e criada para testar a _pipeline_ de CI/CD
do GitLab.

| Descrição                  | URL de acesso                  |
| -------------------------- | ------------------------------ |
| Administração do Django    | http://localhost:8000/admin    |
| API REST (root)            | http://localhost:8000/api/     |
| Documentação (Swagger)     | http://localhost:8000/swagger/ |