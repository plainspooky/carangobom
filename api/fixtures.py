from typing import Callable
from uuid import uuid4

from pytest import fixture
from rest_framework.authtoken.models import Token


@fixture
def api_client():
    """Enable acccess to Django REST framework's client."""

    from rest_framework.test import APIClient

    return APIClient()


@fixture
def create_user(db, django_user_model) -> Callable:
    """Send a callable object that creates a Django's user on test
    database."""

    def make_user(**kwargs):
        """Create a new user on Django's test database"""

        kwargs["password"] = "strong-test-pass"

        if "username" not in kwargs:
            kwargs["username"] = str(uuid4())

        return django_user_model.objects.create_user(**kwargs)

    return make_user


@fixture
def get_or_create_token(db, create_user) -> str:
    """Get or create an access token."""

    user = create_user()
    token, _ = Token.objects.get_or_create(user=user)

    return token
