"""
Views that implement the Rest API.
"""
# import django_filters.rest_framework
# import django_filters.rest_framework
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, viewsets
from rest_framework.permissions import IsAuthenticated

from data.models import Brand, Category, Vehicle

from .serializers import BrandSerializer, CategorySerializer, VehicleSerializer


class BaseViewSetMixin:
    """Base ViewSet to hold common parameters."""

    filter_backends = [DjangoFilterBackend, filters.SearchFilter]
    http_method_names = ["options", "get", "post", "put", "delete"]
    permission_classes = [
        IsAuthenticated,
    ]


class BrandViewSet(BaseViewSetMixin, viewsets.ModelViewSet):
    """Handles with vehicles' brands."""

    __BRAND_FIELDS_TO_USE = [
        "name",
    ]

    queryset = Brand.objects.all()
    serializer_class = BrandSerializer

    filterset_fields = __BRAND_FIELDS_TO_USE
    search_fields = __BRAND_FIELDS_TO_USE


class CategoryViewSet(BaseViewSetMixin, viewsets.ModelViewSet):
    """Handles with vehicles' categories."""

    __CATEGORY_FIELDS_TO_USE = [
        "name",
    ]

    queryset = Category.objects.all()
    serializer_class = CategorySerializer

    filterset_fields = __CATEGORY_FIELDS_TO_USE
    search_fields = __CATEGORY_FIELDS_TO_USE


class VehicleViewSet(BaseViewSetMixin, viewsets.ModelViewSet):
    """Handles with vehicles.

    It supports filtering, ordering, pagination and searhing."""

    __VEHICLE_FIELDS_TO_USE = [
        "model",
        "brand",
        "category",
        "year",
    ]
    __VEHICLE_FIELDS_TO_SEARCH = [
        "model",
        "year",
    ]

    queryset = Vehicle.objects.all()
    serializer_class = VehicleSerializer

    filterset_fields = __VEHICLE_FIELDS_TO_USE
    ordering_fields = __VEHICLE_FIELDS_TO_USE
    search_fields = __VEHICLE_FIELDS_TO_SEARCH

    def get_permissions(self):
        """Override original `GET` method to doesn`t require authentication."""

        if self.action == "list":
            permission_classes = []
        else:
            permission_classes = self.permission_classes

        return [permission() for permission in permission_classes]
