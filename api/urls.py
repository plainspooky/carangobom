from django.urls import include, path
from rest_framework import routers

from . import views

# register REST API routes
router = routers.DefaultRouter()

for route, viewset in (
    (r"brand", views.BrandViewSet),
    (r"category", views.CategoryViewSet),
    (r"vehicle", views.VehicleViewSet),
):
    router.register(route, viewset)

urlpatterns = [
    path("", include(router.urls)),
]
