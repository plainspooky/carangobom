from django.urls import reverse
from pytest import mark
from rest_framework import status

from data.fixtures import FAKE_BRAND, FAKE_VEHICLE

from .fixtures import api_client, create_user, get_or_create_token  # noqa F401


class TestBrandRestApi:
    """Test Brand's Rest API."""

    NEEDING_AUTH = {
        "brand-list": None,
        "brand-detail": {"pk": 1},
    }

    @mark.django_db
    @mark.parametrize("auth_url", NEEDING_AUTH.keys())
    def test_if_cant_access_routes_without_authentication(
        self, api_client, auth_url  # noqa F811
    ) -> None:

        params = self.NEEDING_AUTH.get(auth_url)
        url = reverse(auth_url, kwargs=params)

        response = api_client.get(url)

        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    @mark.django_db
    @mark.parametrize("auth_url", NEEDING_AUTH.keys())
    def test_if_can_access_routes_using_authentication(
        self, api_client, auth_url, get_or_create_token  # noqa F811
    ) -> None:
        """Test if an URL that needs authentication can't be reached without
        previous authentication."""

        params = self.NEEDING_AUTH.get(auth_url)
        url = reverse(auth_url, kwargs=params)

        token = get_or_create_token
        api_client.credentials(HTTP_AUTHORIZATION="Token " + token.key)

        response = api_client.get(url)

        assert response.status_code in (
            status.HTTP_200_OK,
            status.HTTP_404_NOT_FOUND,
        )

    @mark.django_db
    def test_if_can_create_new_brands(
        self, api_client, get_or_create_token  # noqa F811
    ) -> None:

        auth_url = "brand-list"

        url = reverse(auth_url)
        token = get_or_create_token
        api_client.credentials(HTTP_AUTHORIZATION="Token " + token.key)

        response = api_client.post(url, data=FAKE_BRAND)

        assert response.status_code == status.HTTP_201_CREATED


class TestVehicleRestApi:

    NOT_NEEDING_AUTH = {
        "vehicle-list": None,
    }

    NEEDING_AUTH = {
        "vehicle-detail": {"pk": 2},
    }

    WRONG_DATA = {
        "model": 0,
        "year": -1,
        "brand": 0,
    }

    @mark.django_db
    @mark.parametrize("no_auth_url", NOT_NEEDING_AUTH.keys())
    def test_if_cant_access_routes_when_authentication_is_needed(
        self, api_client, no_auth_url  # noqa F811
    ) -> None:

        url = reverse(no_auth_url)
        response = api_client.get(url)

        assert response.status_code == status.HTTP_200_OK

    @mark.django_db
    @mark.parametrize("auth_url", NEEDING_AUTH.keys())
    def test_if_access_routes_using_user_authentication(
        self, api_client, auth_url  # noqa F811
    ) -> None:

        params = self.NEEDING_AUTH.get(auth_url)
        url = reverse(auth_url, kwargs=params)
        response = api_client.get(url)

        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    @mark.django_db
    @mark.parametrize("auth_url", NEEDING_AUTH.keys())
    def test_authenticated_requests(
        self, api_client, auth_url, get_or_create_token  # noqa F811
    ) -> None:

        params = self.NEEDING_AUTH.get(auth_url)
        url = reverse(auth_url, kwargs=params)
        token = get_or_create_token
        api_client.credentials(HTTP_AUTHORIZATION="Token " + token.key)

        response = api_client.get(url)

        assert response.status_code == status.HTTP_404_NOT_FOUND

    @mark.django_db
    @mark.parametrize("wrong_data", WRONG_DATA.keys())
    def test_if_cant_create_new_vehicle_with_invalid_field_content(
        self, api_client, get_or_create_token, wrong_data  # noqa F811
    ) -> None:

        auth_url = "vehicle-list"

        url = reverse(auth_url)
        token = get_or_create_token
        api_client.credentials(HTTP_AUTHORIZATION="Token " + token.key)

        response = api_client.post(
            url,
            data={**FAKE_VEHICLE, **{wrong_data: self.WRONG_DATA[wrong_data]}},
        )

        assert response.status_code == status.HTTP_400_BAD_REQUEST
