from rest_framework import serializers

from data.models import Brand, Category, Vehicle


class BrandSerializer(serializers.ModelSerializer):
    """Serialize Brand data."""

    class Meta:
        model = Brand
        fields = "__all__"


class CategorySerializer(serializers.ModelSerializer):
    """Serialize Category data."""

    class Meta:
        model = Category
        fields = "__all__"


class VehicleSerializer(serializers.ModelSerializer):
    """Serialize Vehicle data."""

    brand = BrandSerializer(many=False, read_only=False)
    category = CategorySerializer(many=False, read_only=False)

    class Meta:
        model = Vehicle
        fields = (
            "id",
            "model",
            "category",
            "brand",
            "year",
        )

    def __lower_or_equal_a_zero(self, value_to_check):
        return value_to_check <= 0

    def validate_year(self, year):

        if self.__lower_or_equal_a_zero(year):
            raise serializers.ValidationError("Year isn't valid.")

        return year
