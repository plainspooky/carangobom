from pytest import fixture

from .models import Brand, Category, Vehicle

FAKE_BRAND = {
    "name": "Avtovaz",
}

FAKE_CATEGORY = {
    "name": "SUV",
}

FAKE_VEHICLE = {
    "model": "Niva Legend 3 doors",
    "value": 32955.0,
    "year": 2021,
}
"""I'm using this car here:
https://www.lada.ru/en/cars/niva-legend/3dv/about.html"""


@fixture
def fake_brand() -> Brand:
    """Create a `Brand` object using fake data."""

    return Brand(**FAKE_BRAND)


@fixture
def fake_category() -> Category:
    """Create a `Category` object using fake data."""

    return Category(**FAKE_BRAND)


@fixture
def fake_vehicle() -> Vehicle:
    """Create a`Vehicle` object using fake data."""

    fake_brand = Brand(**FAKE_BRAND)
    fake_category = Category(**FAKE_CATEGORY)

    return Vehicle(
        {**FAKE_VEHICLE, **{"brand": fake_brand, "category": fake_category}}
    )
