"""
Data models
"""
from django.db import models


class Brand(models.Model):
    """Vehicle's brand name."""

    name = models.CharField(max_length=100)

    class Meta:
        ordering = ("name",)

    def __str__(self) -> str:
        return self.name


class Category(models.Model):
    """Vehicle's category type."""

    name = models.CharField(max_length=100)

    class Meta:
        ordering = ("name",)
        verbose_name_plural = "categories"

    def __str__(self) -> str:
        return self.name


class Vehicle(models.Model):
    """Vehicle data."""

    model = models.CharField(max_length=100)
    year = models.IntegerField()

    brand = models.ForeignKey(
        Brand, on_delete=models.CASCADE, related_name="vehicle_brand"
    )
    category = models.ForeignKey(
        Category, on_delete=models.CASCADE, related_name="vehicle_category"
    )

    class Meta:
        ordering = (
            "brand",
            "model",
        )

    def __str__(self) -> str:
        return self.model
