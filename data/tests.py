from pytest import mark

from .fixtures import fake_brand, fake_category, fake_vehicle  # noqa F401
from .models import Brand, Category, Vehicle


class TestModelsBrand:
    """Test `Brand` model."""

    @mark.django_db
    def test_if_create_a_new_instance(self, fake_brand) -> None:  # noqa F811

        assert isinstance(fake_brand, Brand)

    @mark.django_db
    def test_if_str_of_brand_is_same_if_name_fields(
        self, fake_brand  # noqa F811
    ) -> None:

        assert str(fake_brand) == fake_brand.name


class TestModelsCategory:
    """Test `Category` model."""

    @mark.django_db
    def test_if_create_a_new_instance(
        self, fake_category
    ) -> None:  # noqa F811

        assert isinstance(fake_category, Category)

    @mark.django_db
    def test_if_str_of_category_is_same_if_name_fields(
        self, fake_category  # noqa F811
    ) -> None:

        assert str(fake_category) == fake_category.name


class TestModelVehicle:
    """Test `Vehicle` model."""

    @mark.django_db
    def test_new_instance_of_vehicle(self, fake_vehicle) -> None:  # noqa F811

        assert isinstance(fake_vehicle, Vehicle)

    @mark.django_db
    def test_if_str_of_vehicles_is_its_model(
        self, fake_vehicle  # noqa F811
    ) -> None:

        assert str(fake_vehicle) == fake_vehicle.model
