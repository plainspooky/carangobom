from django.contrib import admin  # noqa F401

from .models import Brand, Category, Vehicle


@admin.register(Brand)
class BrandAdmin(admin.ModelAdmin):
    """Admin class to customize `Brand` for Administration interface."""

    list_display = ("name",)
    list_display_links = ("name",)
    search_fields = ("name",)


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    """Admin class to customize `Category` for Administration interface."""

    list_display = ("name",)
    list_display_links = ("name",)
    search_fields = ("name",)


@admin.register(Vehicle)
class VehicleAdmin(admin.ModelAdmin):
    """Admin class to customize `Vehicle`for Administration interface."""

    list_display = (
        "model",
        "brand",
        "category",
        "year",
    )
    list_display_links = ("model",)
    list_filter = (
        "brand",
        "year",
    )
    search_fields = (
        "name",
        "brand",
        "year",
    )
