.PHONY: coverage

default:
	make runserver

runserver:
	@echo Starting development server
	python ./manage.py runserver

check:
	python ./manage.py check

style_check:
	flake8 --exclude=py3 --show-source --statistics
	flake8 --count --exclude=py3 --exit-zero --max-complexity=10 --statistics

test:
	pytest

test_coverage:
	pytest --cov-report=xml
