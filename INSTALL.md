# Instalação

Instale a versão 3.8 do Python (ou posterior) e digite:

1. Clone o repositório:
  ``` shell
  git clone https://gitlab.com/plainspooky/carangobom.git
  cd carangobom
  ```

1. Crie e acesse um ambiente virtual:

  ``` shell
  python3 -m venv py3
  source ./p3/bin/activate
  ```
  
  No caso de estar utilizando Windows, faça:
  
  ``` shell
  python -m venv py3
  py3\scripts\activate
  ``` 
    
1. Instale as dependências do projeto:

  ``` shell
  cd src
  pip install --requirement requirements.txt
  ```

1. Configure o banco de dados e importe a base de testes:

  ``` shell
  python manage.py migrate
  python manage.py loaddata sample/carangobom.json
  ```

1. Crie o super usuário:

  ``` shell
  python manage.py createsuperuser 
  ```
  
  E preencha os campos a seguir:
  
  ``` console
  Username (leave blank to use '« seu login »'): 
  Email address: « seu endereço de e-mail »
  Password: 
  Password (again): 
  Superuser created successfully.
  ```

1. Inicie o servidor de testes com:

  ``` shell
  python manage.py runserver
  ```